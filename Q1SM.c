#include "scresposta.h"

int main() {
    Pessoa*twitter = build_graph_UX();
    char nome[10];
    printf("Quem deseja checar?\n");
    read_name(nome);
    int n = followed_number(twitter, nome);
    printf("A pessoa %s segue %d outras pessoas!\n", nome, n);

    free_graph(twitter);
    return 0;
}