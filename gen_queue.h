#ifndef GENQUEUE_H
#define GENQUEUE_H

#include <stdlib.h>
#include <stdio.h>

typedef struct lista { // Estrutura de fila genérica(lista dinamica)
    void * node;// Cada nó guarda um ponteiro genérico
    struct lista * prox; // Ponteiro para o próximo da fila
}List;

typedef struct queue { //Descritor da fila
    List * ini; // Ponteiro para o início da fila
    List * fim; // Ponteiro para o fim da fila
}QUE;

QUE * create_q (void);
void insert_q( QUE * f, void * node);
void * withdraw_q(QUE * f);
int empty_q(QUE * f);
void free_q(QUE * f);
void run_que(QUE * f, void (*cb)(void*));
void run_que_2(QUE * f, void (*cb)(void*, void*), void * dado);
void run_que_3(QUE * f, void (*cb)(void*, void*, void*), void * dado, void * valor);
void remove_q(QUE*f, void*p);
List * find_q(QUE*f, void*p);
int compare(QUE*f, QUE*p, int (*cb)(void*, void*));


QUE * create_q (void) {
    QUE * f = (QUE *) malloc(sizeof(QUE)); // Aloca espaço para a fila
    f->ini = f->fim = NULL; // Ponteiros de início e fim são anulados
    return f; // Retorna a fila
}

void insert_q( QUE * f, void * node) {
    List * n = (List *)malloc(sizeof(List)); // Aloca espaço para o nó
    n->node = node; // Insere endereço para o campo node genérico
    n->prox = NULL; // Ponteiro para o próximo é anulado
    if(f->fim) { // Se existir um fim, ou seja, a fila não está fazia
        f->fim->prox = n; // O nó que estava no fim, seu campo próximo aponta para n,
        f->fim = n; // O ponteiro fim passa a apontar para n
    }
    else { // Caso a fila esteja vazia
        f->ini = n; // O começo apontará para n
        f->fim = n; // O fim também apontará para n
    }
}

void * withdraw_q(QUE * f) {
    List * t; // Inicia ponteiro auxiliar
    void * node; // Inicia valor para retornar
    if(empty_q(f)) {
        printf("Fila vazia!\n");
        return NULL; // Retorna para onde a função foi chamada
    }
    t = f->ini; // Ponteiro t aponta para o primeiro nó
    node = t->node; // Valor recebe o que está no campo valor do nó t
    f->ini = t->prox; // Primeiro da fila passa a ser o segundo nó da fila
    if(!f->ini) { // Se a fila ficar vazia
        f->fim = NULL; // O fim se tornará nulo também
    }
    free(t); // Libera o nó de t, que ocupa a primeira posição na fila
    return node; // Retorna valor que estava no nó
}

int empty_q(QUE * f) {
    return (f->ini == NULL); // Retorna se o começo da fila existe ou não
}

void free_q(QUE * f) {
    if(f->ini == NULL){
        free(f); // Libera a estrutura fila, com os campos ini e fim
        return;
    }
    List * q = f->ini; // Cria um nó para iteração, apontando para o começo da fila
    List * t; // Cria um ponteiro auxiliar
    while(q) {
        t = q->prox; // Ponteiro auxiliar recebe o próximo de q
        free(q); // Libera q
        q = t; // Atualiza q com o valor de seu próximo, que estava salvo em t
    }
    free(f); // Libera a fila
}

/*Abaixo estão as funções que percorrem a fila com auxílio da callback para operar
os nós. Note que por usarmos ponteiros genéricos, não temos acesso aos campos dos nós
para o qual ->node aponta, para isso precisamos de uma função(callback) que usa typecast
no endereço armazenado no nó para operar. Callback é um ponteiro para função, que deve
ser inicializado com a mesma assinatura da função a qual deseja representar*/

void run_que(QUE * f, void (*cb)(void*)) { //Função para percorrer com uso de callback
    List * q; // Inicia variável de iteração
    for(q=f->ini; q; q=q->prox) cb(q->node); //A cada iteração, chama o callback
}

void run_que_2(QUE * f, void (*cb)(void*, void*), void * dado) {
    //Caso precise manipular um dado adicional, como vetor, struct, etc
    List * q; // Inicia variável de iteração
    for(q=f->ini; q; q=q->prox) cb(q->node, dado); //A cada iteração, chama o callback
}

void run_que_3(QUE * f, void (*cb)(void*, void*, void*), void * dado, void * valor) {
    //Caso precise manipular um outro dado adicional, como posição do vetor, etc
    List * q; // Inicia variável de iteração
    for(q=f->ini; q; q=q->prox) cb(q->node, dado, valor);//A cada iteração,chama o callback
}

void remove_q(QUE*f, void*p) {
    List*l = f->ini, *ant=NULL;//Inicia ponteiro auxiliar e anterior
    //Enquanto não tiver encontrado, atualiza o auxiliar e o anterior
    while((l) && (l->node != p)) { ant=l; l=l->prox; }
    if(!l) return;//Se não encontrou, retorna a lista inalterada
    //Se o nó é o primeiro, atualiza o campo início da fila
    if(l == f->ini) f->ini = l->prox;
    //Se o nó é o último, atualiza o campo fim da fila
    if(l == f->fim) f->fim = ant;
    //Amarra o próximo do anterior com o próximo do nó a ser removido
    if(ant) ant->prox = l->prox;
    free(l);//Libera o nó desejado
    return;
}

List * find_q(QUE*f, void*p) {
    List*l=f->ini; //Inicia ponteiro para iteração
    //Enquanto o ponteiro estiver dentro da fila e o campo node,
    //for diferente do nó que buscamos, atualiza para o próximo
    while((l) && (l->node!=p)) l=l->prox;
    //Se encontrar, retorna um ponteiro válido, caso não tenha encontrado
    //retorna NULL
    return l;
}

int compare(QUE*f, QUE*p, int (*cb)(void*, void*)) {
    List*a=f->ini, *b=p->ini;//Inicia nodes para iteração
    //A cada iteração compara usando o critério de comparação cb
    while(a&&b){if(!cb(a->node, b->node))return 0; a=a->prox;b=b->prox;}
    //Se terminaram juntos e até então são iguais
    if(!a && !b) return 1;
    //Se são iguais até um ponto, mas um é maior que o outro
    return 0;
}

#endif //GENQUEUE_H
