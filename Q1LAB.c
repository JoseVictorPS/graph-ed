#include "respostas.h"

int main() {
    TG*grafo = build_graph_UX();
    int n = read_value();
    int r = grau_grafo(grafo, n);
    print_grau(r);

    free_graph(grafo);
    return 0;
}