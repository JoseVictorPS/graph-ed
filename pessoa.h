#ifndef PESSOA_H
#define PESSOA_H

#include "gen_queue.h"
#include <string.h>

typedef struct grafo {
    int idd;
    char nome[10];
    struct grafo *prox;
    QUE * seg;
}Pessoa;

static Pessoa * initialize();
int void_graph(Pessoa * g);
static Pessoa * insert(Pessoa * g, int idd, char*nome);
static void follow_people(Pessoa*g);
static void insert_followed_UX(Pessoa*g, Pessoa*seg);
static Pessoa * busca_node(Pessoa*g, char*v);
Pessoa * build_graph_UX();
void free_graph(Pessoa * g);
void print_graph(Pessoa * g);
static void print_cb(void*g);
static Pessoa*remove_node(Pessoa*g, char* n);
static void remove_followed(Pessoa*g, Pessoa*f);
Pessoa * remove_from_UX(Pessoa*g);
static void read_name(char*c);

static Pessoa * initialize() { return NULL; } //Inicia ponteiro como NULL

int void_graph(Pessoa * g){ return g==NULL; }//Se apontar para NULL é vazio

static Pessoa * insert(Pessoa * g, int idd, char*nome) {
    Pessoa *novo =(Pessoa *)malloc(sizeof(Pessoa));//Aloca espaço na memória
    novo->prox = g;//Atribui o próximo
    novo->idd = idd;//Atribui o campo idade
    strcpy(novo->nome, nome);//Atribui o campo nome
    novo->seg = create_q();//Inicia uma fila de seguidos
    return novo;//Retorna o nó para ser o primeiro da lista de grafos
}

static void read_name(char*c) {
    printf("Digite o nome: ");
    char nome[10];//Inicia string para leitura
    scanf("%s", nome); //10[^\n]
    strcpy(c, nome);//Copia na string c o valor lido
}

static Pessoa * busca_node(Pessoa*g, char*v) {
    if(!g) return NULL; //Se ponteiro para grafo for inválido
    if(!strcmp(g->nome, v)) return g; //se encontrou, retorna o nó em questão
    return busca_node(g->prox, v);//Busca no próximo nó
}

Pessoa * build_graph_UX() {
    Pessoa * g = initialize();//Atribui NULL para o ponteiro
    printf("Deseja inserir um node no grafo?  ");
    int op, idd;//Inicia variável de operação e idade
    char n[10];//Inicia string nome
    scanf("%d", &op);//lê variável de operação
    while(op) {
        printf("Qual a idade da pessoa?  ");
        scanf("%d", &idd);//lê a idade
        read_name(n);//lê o nome
        g=insert(g, idd, n);//Insere o nó na lista de grafos
        print_graph(g);//Imprime a lista de grafos
        printf("Deseja inserir novamente?  ");
        scanf("%d", &op);//para operar novamente
    }
    follow_people(g);
    print_graph(g);
    return g;//Retorna a lista de grafos
}

static void insert_followed_UX(Pessoa*g, Pessoa*seg) {
    int op=1;//Inicia valor para operação como verdade
    char n[10];//Inicia string nome
    while(op) {
        printf("SEGUIDOS:\n");
        read_name(n);//Lê nome de quem vai seguir
        Pessoa*alvo = busca_node(g, n);//Busca se o nó existe
        //Insere o nó buscado na fila de seguidos
        if(alvo) insert_q(seg->seg, alvo);
        else printf("Node nao encontrado!\n");
        printf("Inserir outro seguido?  ");
        scanf("%d", &op); //para operar novamente
    }
}

static void follow_people(Pessoa*g) {
    char n[10];//Inicia string pra ler nome
    Pessoa*seg;//Inicia seguidor
    int op;//Inicia variavel de interação
    printf("Ha seguidores? ");
    scanf("%d", &op);//lê variavel de interação
    while(op) {
        read_name(n);//lê nome do seguidor
        seg=busca_node(g, n);//Busca o seguidor no grafo
        if(seg)insert_followed_UX(g, seg);//Preenche os seguidores com UX
        else printf("Seguidor nao encontrado!\n");
        printf("Ha mais seguidores? ");
        scanf("%d", &op);
    }
}

void free_graph(Pessoa * g) {
    if(!g) return;//Se o ponteiro for inválido, não há o que liberar
    Pessoa*t;//Inicia ponteiro auxiliar
    while(g) {
        t = g;//Atribui o valor do nó de iteração ao auxiliar
        g = g->prox;//Atualiza o nó de iteração para o próximo
        free_q(t->seg);//Libera a fila deseguidos do nó auxiliar
        free(t);//Libera nó auxiliar
    }
}

void print_graph(Pessoa * g) {
    Pessoa*t = g;//Inicia nó para iteração
    while(t) {
        printf("%s(%d)->  ", t->nome, t->idd);
        run_que(t->seg, print_cb);//Imprime os seguidos
        printf("\n");
        t = t->prox;//Atualiza o nó de iteração
    }
}

static void print_cb(void*g) {
    Pessoa*t = (Pessoa*)g;//Typecast no ponteiro para acessar seus campos
    printf("%s(%d)  ", t->nome, t->idd);
}

static Pessoa*remove_node(Pessoa*g, char* n) {
    Pessoa*f = g, *ant=NULL;//Inicia ponteiro auxiliar, e ponteiro anterior
    //Enquanto não tiver encontrado, atualiza o auxiliar e o anterior 
    while((f) && (strcmp(f->nome, n))) { ant=f; f=f->prox; }
    if(!f) return g;//Se não encontrou, retorna a lista inalterada
    if(!ant) g=g->prox;//Se o ponteiro que queremos é o primeiro
    //Amarra o próximo do anterior com o próximo do nó a ser removido
    else ant->prox = f->prox;
    free_q(f->seg);//Libera a fila de seguidos do nó a ser removido
    remove_followed(g, f);//Libera o nó da fila de seguidos dos outros nós
    free(f);//Libera o nó
    return g;//Retorna o grafo
}

static void remove_followed(Pessoa*g, Pessoa*f) {
    //percorre a lista de grafos removendo o nó f das filas de seguido
    while(g) { remove_q(g->seg, f); g=g->prox; }
}

Pessoa * remove_from_UX(Pessoa*g) {
    printf("Deseja remover um node no grafo?  ");
    int op;//Lê variáveis de operação e remoção
    char n[10];//Inicia string de nome
    scanf("%d", &op);//lê variável de operação
    while(op) {
        read_name(n);//lê o nome
        g=remove_node(g, n);//remove o nó com aquele nome
        print_graph(g);//Imprime o grafo
        printf("Deseja remover novamente?  ");
        scanf("%d", &op);//para operar novamente
    }
    return g;//Retorna a lista
}

#endif // PESSOA_H