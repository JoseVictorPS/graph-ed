#ifndef SCRESPOSTA_H
#define SCRESPOSTA_H

#include "pessoa.h"

int followed_number(Pessoa*g, char*nome);
static void incrementa(void*n, void*i);
int followers(Pessoa*g, char*nome, int impr);
static void cb_follower(void*g, void*p, void*s);
static void impr_follower(void*g, void*p, void*t);
Pessoa*most_popular(Pessoa*g);
int follow_older(Pessoa*p, int impr);
static void cb_older(void*seg, void*t, void*c);

int followed_number(Pessoa*g, char*nome) {
    Pessoa*p=busca_node(g, nome);//Busca a pessoa que desejamos conhecer
    int i=0;//Inicia contador de seguidos
    run_que_2(p->seg, incrementa, &i);//Percorre fila de seguidos
    return i;//Retorna contador
}

static void incrementa(void*n, void*i) {
    int*t=(int*)i;//Typecast para incrementar
    (*t)++;//Incrementa a cada seguido
}

int followers(Pessoa*g, char*nome, int impr) {
    Pessoa*t=g;
    Pessoa*p=busca_node(g, nome);//Acha a pessoa que desejamos
    int s=0;//Inicia contador de seguidores
    //Conta seguidores
    while(t) {run_que_3(t->seg, cb_follower, p, &s); t=t->prox;}
    t=g;//Reseta ponteiro para iteração
    //Se necessário, imprime seguidores
    if(impr)while(t){run_que_3(t->seg, impr_follower, p, t); t=t->prox;}
    printf("\n");
    return s;
}

static void cb_follower(void*g, void*p, void*s) {
    Pessoa*a=(Pessoa*)g;//Typecast para acessar o campo do no
    Pessoa*b=(Pessoa*)p;//Typecast para acessar o campo do no
    int*i = (int*)s;//Typecast para incrementar
    if(!strcmp(a->nome, b->nome))(*i)++;//Se encontrou a pessoa, incrementa
}

static void impr_follower(void*g, void*p, void*t) {
    Pessoa*a=(Pessoa*)g;//Typecast para acessar o campo do no
    Pessoa*b=(Pessoa*)p;//Typecast para acessar o campo do no
    Pessoa*c=(Pessoa*)t;//Typecast para acessar o campo do no
    if(!strcmp(a->nome, b->nome))printf("%s  ", c->nome);
}

Pessoa*most_popular(Pessoa*g) {
    Pessoa*t=g;//Inicializa ponteiro para iteração
    Pessoa*r=g;//Inicializa ponteiro auxiliar de iteração
    Pessoa*pop;//Inicializa ponteiro de retorno
    int s=0;//Inicia contador de seguidores
    int aux; //Inicia contador auxiliar
    while(r) {
        aux=0;//marca contador para 0 a cada nó
        //Percorre por todas as filas de seguidos pra achar os seguidores
        while(t) {run_que_3(t->seg, cb_follower, r, &aux); t=t->prox;}
        //Se a pessoa tiver mais seguidores que 's' ela é a mais popular
        if(aux>s) { s=aux; pop=r; }
        r=r->prox;//Atualiza r
        t=g;//Reseta t
    }
    return pop;
}

int follow_older(Pessoa*p, int impr) {
    Pessoa*t=p;
    int s=0;//Inicia contador de pessoas que só seguem mais velhas
    int c=1;//Inicia booleano como verdade para segue mais velhas
    while(t) {
        //Itera checando se os seguidos são todos mais velhos
        run_que_3(t->seg, cb_older, t, &c);
        //Se sim, incrementa contador, e se for pra imprimir, imprime
        if(c) { s++; if(impr) printf("%s  ", t->nome); }
        else c=1;//Reseta booleano para testar o próximo nó
        t=t->prox;//Atualiza nó de iteração
    }
    printf("\n");
    return s;
}

static void cb_older(void*seg, void*t, void*c) {
    Pessoa*a=(Pessoa*)seg;//Typecast para acessar campos do nó
    Pessoa*b=(Pessoa*)t;//Typecast para acessar campos do nó
    int*d=(int*)c;//Typecast para alterar valor do booleano
    //Se o seguido não for mais velho, booleano marca falso
    if(b->idd >= a->idd) (*d)=0;
}

#endif //SCRESPOSTA_H