#ifndef GRAFO_H
#define GRAFO_H

#include "gen_queue.h"

typedef struct grafo {
    int info;
    struct grafo *prox;
    QUE * viz;
}TG;

static TG * initialize();
int void_graph(TG * g);
static TG * insert(TG * g, int n);
static void insert_neighbors_UX(TG*g);
static TG * busca_node(TG*g, int v);
TG * build_graph_UX();
void free_graph(TG * g);
void print_graph(TG * g);
static void print_cb(void*g);
static TG*remove_node(TG*g, int n);
static void remove_neighbors(TG*g, TG*f);
TG * remove_from_UX(TG*g);

static TG * initialize() { return NULL; } //Inicia ponteiro como NULL

int void_graph(TG * g){ return g==NULL; }//Se apontar para NULL é vazio

static TG * insert(TG * g, int n) {
    TG *novo = (TG *) malloc(sizeof(TG));//Aloca espaço na memória
    novo->prox = g;//Atribui o próximo
    novo->info = n;//Atribui o valor que carregará
    novo->viz = create_q();//Inicia uma fila de vizinhos
    insert_neighbors_UX(novo);//Insere vizinhos com interação com usuário
    return novo;//Retorna o nó para ser o primeiro da lista de grafos
}

static void insert_neighbors_UX(TG*g) {
    printf("Deseja inserir um vizinho?  ");
    int op, v;//Inicia valor para operação e valor que o vizinho carregará
    scanf("%d", &op);//lê o valor para operação
    while(op) {
        printf("Qual o valor de node sera o vizinho?  ");
        scanf("%d", &v); //lê o valor que o nó carregará
        TG*alvo = busca_node(g, v);//Busca se o nó existe
        //Insere o nó buscado na fila de vizinhos, e insere
        //O nó em questão na fila de vizinhos do nó buscado
        if(alvo){ insert_q(g->viz, alvo); insert_q(alvo->viz, g); }
        else printf("Node nao encontrado!\n");
        printf("Inserir outro vizinho?  ");
        scanf("%d", &op); //para operar novamente
    }
}

static TG * busca_node(TG*g, int v) {
    if(!g) return NULL; //Se ponteiro para grafo for inválido
    if(g->info==v) return g; //se encontrou, retorna o nó em questão
    return busca_node(g->prox, v);//Busca no próximo nó
}

TG * build_graph_UX() {
    TG * g = initialize();//Atribui NULL para o ponteiro
    printf("Deseja inserir um node no grafo?  ");
    int op, v;//Inicia variável de operação e para inserção no nó
    scanf("%d", &op);//lê variável de operação
    while(op) {
        printf("Qual valor deseja inserir?  ");
        scanf("%d", &v);//lê variável para inserção
        g=insert(g, v);//Insere o nó na lista de grafos
        print_graph(g);//Imprime a lista de grafos
        printf("Deseja inserir novamente?  ");
        scanf("%d", &op);//para operar novamente
    }
    return g;//Retorna a lista de grafos
}

void free_graph(TG * g) {
    if(!g) return;//Se o ponteiro for inválido, não há o que liberar
    TG*t;//Inicia ponteiro auxiliar
    while(g) {
        t = g;//Atribui o valor do nó de iteração ao auxiliar
        g = g->prox;//Atualiza o nó de iteração para o próximo
        free_q(t->viz);//Libera a fila de vizinhos do nó auxiliar
        free(t);//Libera nó auxiliar
    }
}

void print_graph(TG * g) {
    TG*t = g;//Inicia nó para iteração
    while(t) {
        printf("%d ->  ", t->info);
        run_que(t->viz, print_cb);//Imprime os vizinhos
        printf("\n");
        t = t->prox;//Atualiza o nó de iteração
    }
}

static void print_cb(void*g) {
    TG*t = (TG*)g;//Typecast no ponteiro para acessar seus campos
    printf("%d  ", t->info);
}

static TG*remove_node(TG*g, int n) {
    TG*f = g, *ant=NULL;//Inicia ponteiro auxiliar, e ponteiro anterior
    //Enquanto não tiver encontrado, atualiza o auxiliar e o anterior 
    while((f) && (f->info!=n)) { ant=f; f=f->prox; }
    if(!f) return g;//Se não encontrou, retorna a lista inalterada
    if(!ant) g=g->prox;//Se o ponteiro que queremos é o primeiro
    //Amarra o próximo do anterior com o próximo do nó a ser removido
    else ant->prox = f->prox;
    free_q(f->viz);//Libera a fila de vizinhos do nó a ser removido
    remove_neighbors(g, f);//Libera o nó da fila de vizinhos dos outros nós
    free(f);//Libera o nó
    return g;//Retorna o grafo
}

static void remove_neighbors(TG*g, TG*f) {
    //percorre a lista de grafos removendo o nó f das filas de vizinho
    while(g) { remove_q(g->viz, f); g=g->prox; }
}

TG * remove_from_UX(TG*g) {
    printf("Deseja remover um node no grafo?  ");
    int op, v;//Lê variáveis de operação e remoção
    scanf("%d", &op);//lê variável de operação
    while(op) {
        printf("Qual valor deseja remover?  ");
        scanf("%d", &v);//lê variável de remoção
        g=remove_node(g, v);//remove o nó com aquela informação
        print_graph(g);//Imprime o grafo
        printf("Deseja remover novamente?  ");
        scanf("%d", &op);//para operar novamente
    }
    return g;//Retorna a lista
}

#endif // GRAFO_H