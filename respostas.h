#ifndef RESPOSTA_H
#define RESPOSTA_H

#include "grafo.h"

int number_of_nodes(TG*g);
int number_of_edges(TG*g);
static int edges(TG*g, QUE*v);
static void compara(void*p, void*c, void*v);
int not_same_color(TG*g);
static void color(void*p, void*i, void*cor);
void print_color(int n);
int read_value();
int grau_grafo(TG*g, int n);
static void contagem(void*node, void*c);
static int att_grau(int * r, int * c, int n);
void print_grau(int n);
int equals_g(TG*g, TG*q);
static int cmpviz(void*g, void*q);

int number_of_nodes(TG*g) {
    int c=0; //Inicia contador

    while(g) { c++; g=g->prox; }//Incrementa o contador a cada nó

    return c; //Retorna contador
}

int number_of_edges(TG*g) {
    int c=0; //Inicia contador
    QUE*visited=create_q();//Inicia fila para guardar nós visitados
    //Checa as arestas de cada nó do grafo
    while(g) { c+=edges(g, visited); g=g->prox; }

    return c; //Retorna contador
}

static int edges(TG*g, QUE*v) {
    int c=0;//Inicia contador do nó em questão

    //Percorre a fila de vizinhos e checa se eles já foram visitados
    run_que_3(g->viz, compara, &c, v);
    insert_q(v, g); //Insere o nó em questão na fila de visitados

    return c; //Retorna contador do nó em questão
}

static void compara(void*p, void*c, void*v) {
    int*i = (int*)c; //Typecast no endereço do contador
    if(!find_q(v, p)) (*i)++;//Se o nó não tiver sido visitado,
    //incrementa contador
}

int not_same_color(TG*g) {
    int i=1;//Inicia considerando que todos os vizinhos tem cores diferentes
    while(g) { run_que_3(g->viz, color, &i, g); if(!i)break; g=g->prox; }
    return i;
}

static void color(void*p, void*i, void*cor) {
    TG*viz= (TG*)p; //Typecast no nó vizinho para acessar sua info
    TG*g = (TG*)cor; //Typecast no nó percorrido para acessar sua info
    int*t = (int*)i; //Typecast no ponteiro i para modificar seu valor
    if(g->info == viz->info) (*t) = 0;
}

void print_color(int n) {
    if(n) printf("Os vizinhos no grafo possuem cores diferentes!\n");
    else printf("Há vizinhos com cores iguais!\n");
}

int read_value() {
    int n;//Inicia valor para ser lido
    printf("Qual inteiro deseja usar? ");
    scanf("%d", &n);//Lê valor n
    return n;
}

int grau_grafo(TG*g, int n) {
    int r=1;//Inicia valor de resposta considerando que seja verdade
    int c=0;//Inicia contador de vizinhos
    while(g) {
        run_que_2(g->viz, contagem, &c);//Checa vizinhos de cada nó
        if(att_grau(&r, &c, n)) break;//Atualiza valores
        g=g->prox;//Atualiza iteração
    }

    return r;//Retorna resposta
}

static void contagem(void*node, void*c) {
    int*i = (int*)c;//Typecast para incrementar
    (*i)++;//Incrementa o contador de vizinhos
}

static int att_grau(int * r, int * c, int n) {
    int ret=0;//Inicia valor de retorno como falso
    //Se um no tiver mais vizinhos que o desejado, retorno vira verdade
    //E o valor de r(retorno de grau_grafo) vira falso
    if((*c) != n) { ret=1; (*r)=0; } 
    else (*c) = 0;//Reseta contador para o pŕoximo nó
    return ret;
}

void print_grau(int n) {
    if(n) printf("Os nodes do grafo tem o grau desejado!\n");
    else printf("Ha nodes no grafo com graus indesejados!\n");
}

int equals_g(TG*g, TG*q) {
    while(g && q) {
        //Se a informação na lista de nodes grafos for diferente
        if(q->info != g->info) return 0;
        //Se a fila de vizinhos for diferente
        if(!compare(g->viz, q->viz, cmpviz)) return 0;
        g=g->prox;//Atualiza ponteiro g
        q=q->prox;//Atualiza ponteiro q
    }
    //Se terminaram juntos e até então são iguais
    if(!g && !q) return 1;
    //Se são iguais até um ponto, mas um é maior que o outro
    else return 0;
}

static int cmpviz(void*g, void*q) {
    TG*a=(TG*)g;//Typecast para acessar campo info de g
    TG*b=(TG*)q;//Typecast para acessar campo info de q
    if(a->info != b->info) return 0;//Se os campos forem diferentes
    else return 1;//Se forem iguais
}

void print_equal(int n) {
    if(n)printf("Os grafos sao iguais!\n");
    else printf("Sao diferentes!\n");
}

#endif // RESPOSTA_H