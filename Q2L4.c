#include "respostas.h"

int main() {
    TG*grafo = build_graph_UX();
    int n = number_of_edges(grafo);
    printf("O numero de arestas e:  %d\n", n);

    free_graph(grafo);
    return 0;
}